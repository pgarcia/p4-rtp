## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268 paquetes
* ¿Cuánto tiempo dura la captura?: 12.810068 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes/segundo aproximadamente
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 133
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete: 49154
* SSRC del paquete: 0x31BE1E0E
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642 paquetes
* ¿El origen del flujo F es la máquina A o B?: La máquina B es el origen del flujo F
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: 12.81 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 31.653000 ms
* ¿Cuál es el jitter medio del flujo?: 12.234262 
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes perdidos
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: Esta todo el tiempo por encima de 0.4 ms
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18502
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Tarde
* ¿Qué jitter se ha calculado para ese paquete?: **
* ¿Qué timestamp tiene ese paquete?: 1769316203 
* ¿Por qué número hexadecimal empieza sus datos de audio?: **
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: Oct 16, 2023 10:48:39.269791920 CEST
* Número total de paquetes en la captura: 900 paquetes
* Duración total de la captura (en segundos): 2.9993291 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450 paquetes
* ¿Cuál es el SSRC del flujo?: 0x43dfc99d
* ¿En qué momento (en ms) de la traza comienza el flujo?: 0ms (primera traza)
* ¿Qué número de secuencia tiene el primer paquete del flujo?: 20053
* ¿Cuál es el jitter medio del flujo?: 0.00
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes perdidos
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: j.velles.2019 (Javier Velles)
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): +98 paquetes
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: Iguales (450 paquetes)
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: 20053 - 12321 = 7732
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: La diferencia que encuentro es que en la captura 
* rtp.cap el primer paquete RTP se encuentra en el primer paquete de la captura, y en la rtp2.cap se encuentra en el 
* paquete 99, por lo que cambia el puerto de salida, el SSRC y el start time. 
